A simple Hibernate-based application, which runs CRUD (Create, Read, Update, Delete) operations on user entities by using an instance of a DAO class. 

The application's operational mode is pretty straightforward. First you will need to create a "users" table in MySQL and start the server. Then, use your IDE or the Java console
to run the application. You'll be prompted to enter an option in the console:

1 - Persist a user.
2 - Fetch an existing user.
3 - Update an existing user.
4 - Remove an existing user.

To make sure each operation has been properly executed, use a visual MySQL tool, such as MySQL workbench or just the plain MySQL prompt and check the contents of the "users" table. It's really that simple.